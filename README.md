<a href="https://www.linkedin.com/in/joseantoniogn/">
    <img src="https://content.linkedin.com/content/dam/me/business/en-us/amp/brand-site/v2/bg/LI-Logo.svg.original.svg" alt="LinkedIn" width="70" height="auto">
</a>
|   
<a href="https://orcid.org/0000-0003-2334-1556">
    <img src="https://info.orcid.org/wp-content/uploads/2020/01/orcid-logo.png" alt="ORCID" width="50" height="auto">
</a>
 | 
<a href="https://www.researchgate.net/profile/Jose-Antonio-Gonzalez-Novoa">
    <img src="https://upload.wikimedia.org/wikipedia/commons/a/aa/ResearchGate_Logo.png" alt="Research Gate" width="70" height="auto">
</a>
|
<a href="https://www.webofscience.com/wos/author/record/ABG-2414-2021">
    <img src="https://upload.wikimedia.org/wikipedia/commons/c/c9/Web_of_Science_Logo_12.2023.svg" alt="Web of Science" width="70" height="auto">
</a>

   